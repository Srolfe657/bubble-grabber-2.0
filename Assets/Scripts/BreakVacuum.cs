﻿using UnityEngine;
using System.Collections;

public class BreakVacuum : MonoBehaviour {

	public int breakTime;
	public GameObject vacuumParticles;
	public GameObject catchBubbles;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "FakeVacuum")
		{
			//stop the particle emitter
			// cancel out the ability to click on bubbles for x seconds
			catchBubbles.SetActive(false);
			vacuumParticles.SetActive(false);
			StartCoroutine (RepairVacuum ());
		}
	}

	IEnumerator RepairVacuum()
	{
		yield return new WaitForSeconds (breakTime);
		catchBubbles.SetActive (true);
		vacuumParticles.SetActive(true);
	}
}
