﻿using UnityEngine;
using System.Collections;

public class CatchBubbles : MonoBehaviour
{

    public int score;
    public GameObject bubble;
    
    private Material bubbleColor;

    public int blueBubbleCount = 0;
    public int greenBubbleCount = 0;
    public int orangeBubbleCount = 0;
    public int purpleBubbleCount = 0;
    public int redBubbleCount = 0;
    public int yellowBubbleCount = 0;

    private DecreaseDropSpeed decrease;

    // Use this for initialization
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            // When a bubble is clicked on with the left mouse button
            if (hit.collider.tag == "Bubble" && Input.GetMouseButtonDown(0))
            {
                // Gets the materials name and compares it to the specified color
                bubbleColor = hit.transform.gameObject.GetComponent<Renderer>().material;
                if (bubbleColor.name == "Blue Bubble (Instance)")
                {
                    // Increases the count for the specific color of bubble and logs it
                    blueBubbleCount++;
                    Debug.Log("1 Blue Bubble Captured");
                    Debug.Log("Blue Bubbles Captured: " + blueBubbleCount);
                }

                if (bubbleColor.name == "Green Bubble (Instance)")
                {
                    // Increases the count for the specific color of bubble and logs it
                    greenBubbleCount++;
                    Debug.Log("1 Green Bubble Captured");
                    Debug.Log("Green Bubbles Captured: " + greenBubbleCount);
                }

                if (bubbleColor.name == "Orange Bubble (Instance)")
                {
                    // Increases the count for the specific color of bubble and logs it
                    orangeBubbleCount++;
                    Debug.Log("1 Orange Bubble Captured");
                    Debug.Log("Orange Bubbles Captured: " + orangeBubbleCount);
                }

                if (bubbleColor.name == "Purple Bubble (Instance)")
                {
                    // Increases the count for the specific color of bubble and logs it
                    purpleBubbleCount++;
                    Debug.Log("1 Purple Bubble Captured");
                    Debug.Log("Purple Bubbles Captured: " + purpleBubbleCount);
                }

                if (bubbleColor.name == "Red Bubble (Instance)")
                {
                    // Increases the count for the specific color of bubble and logs it
                    redBubbleCount++;
                    Debug.Log("1 Red Bubble Captured");
                    Debug.Log("Red Bubbles Captured: " + redBubbleCount);
                }

                if (bubbleColor.name == "Yellow Bubble (Instance)")
                {
                    // Increases the count for the specific color of bubble and logs it
                    yellowBubbleCount++;
                    Debug.Log("1 Yellow Bubble Captured");
                    Debug.Log("Yellow Bubbles Captured: " + yellowBubbleCount);
                }

                // Shut gravity off after the bubble is clicked on
                hit.rigidbody.useGravity = false;

                // Zero out the bubbles velocity and angular velocity
                hit.rigidbody.velocity = new Vector3 (0,0,0);
                hit.rigidbody.angularVelocity = new Vector3(0, 0, 0);

                // Push the bubble to the right (into the vacuum)
                hit.rigidbody.AddForce(100, 0, 0);

                // Add 10 points to the current score
                score += 10;
                Debug.Log("Score: " + score);
            }

            if (hit.collider.tag == "WallPowerUp" && Input.GetMouseButtonDown(0))
            {
                // Shut gravity off after the bubble is clicked on
                hit.rigidbody.useGravity = false;

                // Zero out the bubbles velocity and angular velocity
                hit.rigidbody.velocity = new Vector3(0, 0, 0);
                hit.rigidbody.angularVelocity = new Vector3(0, 0, 0);

                // Push the bubble to the right (into the vacuum)
                hit.rigidbody.AddForce(100, 0, 0);
            }

			if (hit.collider.tag == "VacuumBreaker" && Input.GetMouseButtonDown(0))
			{
				// Shut gravity off after the bubble is clicked on
				hit.rigidbody.useGravity = false;

				// Zero out the bubbles velocity and angular velocity
				hit.rigidbody.velocity = new Vector3(0, 0, 0);
				hit.rigidbody.angularVelocity = new Vector3(0, 0, 0);

				// Push the bubble to the right (into the vacuum)
				hit.rigidbody.AddForce(100, 0, 0);
			}


            if (hit.collider.tag == "DecreaseFallSpeedPowerUp" && Input.GetMouseButtonDown(0))
            {
                // Shut gravity off after the bubble is clicked on
                hit.rigidbody.useGravity = false;

                // Zero out the bubbles velocity and angular velocity
                hit.rigidbody.velocity = new Vector3(0, 0, 0);
                hit.rigidbody.angularVelocity = new Vector3(0, 0, 0);

                // Push the bubble to the right (into the vacuum)
                hit.rigidbody.AddForce(100, 0, 0);

                StartCoroutine(decrease.DecreaseFallSpeed());
            }

            // When the bubble is right clicked on
            if (hit.collider.tag == "Bubble" && Input.GetMouseButtonDown(1))
            {
                // Destroy the bubble and log it
                Destroy(hit.rigidbody.gameObject);
                Debug.Log("You popped a bubble :(");
            }

        }

    }

}
