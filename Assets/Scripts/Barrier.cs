﻿using UnityEngine;
using System.Collections;

public class Barrier : MonoBehaviour {

    public GameObject wall;
    public GameObject clone;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SpawnWall()
    {
        clone = Instantiate(wall, new Vector3(0,-11, -5), transform.rotation) as GameObject;
    }
}
