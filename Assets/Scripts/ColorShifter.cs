﻿using UnityEngine;
using System.Collections;

public class ColorShifter : MonoBehaviour {

    public Material[] shiftColors;
    private Material colorShiftBubble;

	// Use this for initialization
	void Start () {
        StartCoroutine(ColorShift());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator ColorShift()
    {
        if (GameObject.Find("Color Shifting Bubble") != null)
        {
            GameObject.Find("Color Shifting Bubble").GetComponent<Renderer>().material = shiftColors[Random.Range(0, 6)];
            yield return new WaitForSeconds(0.5f);
            GameObject.Find("Color Shifting Bubble").GetComponent<Renderer>().material = shiftColors[Random.Range(0, 6)];

        }
        else if (GameObject.Find("Color Shifting Bubble") == null)
        {
            StopCoroutine(ColorShift());
        }
    }
}
