﻿using UnityEngine;
using System.Collections;


public class GameController : MonoBehaviour {

    public Material[] bubbleColors;
    private int levelNumber = 1;

    public int battery = 1;

    public int easyAmount;
    public int bubbleColor;

    // Variable for the cursors texture
    public Texture2D cursorTexture;
    // Sets the variable for the cursor to choose between hardware and software mode
    public CursorMode cursorMode = CursorMode.Auto;
    // Sets the click point on the cursor
    public Vector2 cursorHotSpot;

    public BubbleSpawner bubbleS;

 

    // Use this for initialization
    void Start () {

        bubbleS = gameObject.GetComponent<BubbleSpawner>();

        bubbleColor = Random.Range(0, 6);
        easyAmount = Random.Range(3, 5);

        // Sets the click area of the cursor to the center
        cursorHotSpot = new Vector2(cursorTexture.width / 2, cursorTexture.height / 2);

        // Sets the cursor to the crosshair texture when the game starts
        Cursor.SetCursor(cursorTexture, cursorHotSpot, cursorMode);

        EasyLevelsWinConditions(Random.Range(3, 5), bubbleColors[Random.Range(0, 7)]);

	}
	
	// Update is called once per frame
	void Update () {
     
	}

    void GameOver()
    {
        // Freezes time
        Time.timeScale = 0;
        // Displays lose text
        Debug.Log("You Lose!");
    }

    void WinLevel()
    {
        // Freezes time
        Time.timeScale = 0;
        // Displays win text
        Debug.Log("You Win!");
        winConditionGenerator();
    }

    void winConditionGenerator()
    {
        if (levelNumber <= 15)
        {
            EasyLevelsWinConditions(easyAmount, bubbleColors[bubbleColor]);

            if (easyAmount == 3 && bubbleColor == 1)
            {
                if (bubbleS  != null)
                {
                    WinLevel();
                }
            }
        }

        if (levelNumber > 15 && levelNumber <= 30)
        {
           // MediumLevelsWinConditions();
        }

        if (levelNumber > 30 && levelNumber <= 45)
        {
           // HardLevelsWinConditions();
        }
    }

    void EasyLevelsWinConditions(int amountOfBubbles, Material color)
    {
        Time.timeScale = 1;
        Debug.Log("Amount needed: " + amountOfBubbles + " Color needed: " + color);
    }
}
