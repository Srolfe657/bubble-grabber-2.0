﻿using UnityEngine;
using System.Collections;

public class WallHit : MonoBehaviour {

    public Material orange;
    public Material red;
    public GameObject wall;

    private int wallHitCount = 0;

	// Use this for initialization
	void Start () {
        wall = GameObject.FindWithTag("Wall").GetComponent<GameObject>();
    }
	
	// Update is called once per frame
	void Update () {
        

        if (wallHitCount == 1)
        {
            wall.GetComponent<Renderer>().material = orange;
        }
        if (wallHitCount == 2)
        {
            wall.GetComponent<Renderer>().material = red;
        }
        if (wallHitCount == 3)
        {
            Destroy(wall);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Bubble")
        {
            wallHitCount++;
            Destroy(other.gameObject);
        }
    }
}
