﻿using UnityEngine;
using System.Collections;

public class BubbleSpawner : MonoBehaviour {

    // Bubbles to be spawned
    public GameObject bubble;
    public GameObject clone;

    // Bubble colors assigned when spawned
    public Material[] colors;

    public GameObject[] Powerups;

    public GameObject[] Debuffs;
 
    
    // Use this for initialization
    void Start()
    {

        // Start the spawnBubble method after 1 second then repeat every 2 seconds
        InvokeRepeating("SpawnDrop", 1, 2);
   
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (clone != null)
        {
            // Keeps bubbles from leaving the screen area
            clone.GetComponent<Rigidbody>().position = new Vector3(Mathf.Clamp(clone.transform.position.x, -6.75f, 5.65f), clone.transform.position.y, clone.transform.position.z);

            // Moves the bubbles away from the edges of the screen
            if(clone.transform.position.x <= -6.75f)
            {
                clone.GetComponent<Rigidbody>().AddForce(new Vector3(50, clone.transform.position.x, clone.transform.position.z));
            }
            if (clone.transform.position.x >= 5.65f)
            {
                clone.GetComponent<Rigidbody>().AddForce(new Vector3(-50, clone.transform.position.x, clone.transform.position.z));
            }
        }
    }

    // Create an instance of the bubble prefab and spawn it above the screens veiw
    void SpawnDrop()
    {
        switch (3)
        {
            case 3:
                // Creates a new bubble at a random point on the X axis above the screen
                clone = Instantiate(bubble, new Vector3(Random.Range(-6.5f, 5), 16, -5), transform.rotation) as GameObject;

                // Assigns a randomly selected color from the array
                clone.GetComponent<Renderer>().material = colors[Random.Range(0, 6)];

                // Spawns each bubble with a random amount of X axis force and set Y axis force
                clone.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-15, 15), -5, 0), ForceMode.VelocityChange);
            break;

            case 2:
                // Creates a new bubble at a random point on the X axis above the screen
                clone = Instantiate(Powerups[Random.Range(0,6)], new Vector3(Random.Range(-6.5f, 5), 16, -5), transform.rotation) as GameObject;

                // Spawns each bubble with a random amount of X axis force and set Y axis force
                clone.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-15, 15), -5, 0), ForceMode.VelocityChange);
            break;

            case 1:
                // Creates a new bubble at a random point on the X axis above the screen
                clone = Instantiate(Debuffs[Random.Range(0,4)], new Vector3(Random.Range(-6.5f, 5), 16, -5), transform.rotation) as GameObject;

                // Spawns each bubble with a random amount of X axis force and set Y axis force
                clone.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-15, 15), -5, 0), ForceMode.VelocityChange);
            break;

        }

    }

    

   
}
