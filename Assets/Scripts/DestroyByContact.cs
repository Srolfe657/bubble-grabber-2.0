﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {

    public Barrier barrier;

	// Use this for initialization
	void Start () {
       

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "WallPowerUp")
        {
            barrier.SpawnWall();
            // Destroys a gameobject when it collides with the vacuum
            Destroy(other.gameObject);
        }
		if (other.gameObject.tag == "VacuumBreaker") 
		{
			// Move the powerup gameobject off screen so that it isn't destroyed
			other.transform.position = new Vector3 (20,20,5);
		}
        // Destroys a gameobject when it collides with the vacuum
        Destroy(other.gameObject);
    }
}
